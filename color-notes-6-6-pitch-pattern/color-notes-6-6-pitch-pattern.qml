/*
 * SPDX-License-Identifier: GPL-3.0-only
 * 
 * Color Notes 6-6 Pitch Pattern
 * Version: 1.0.0
 * A MuseScore plugin originally based on MuseScore's "Color Notes" plugin.
 *
 * Copyright (C) 2021 MuseScore BVBA and others
 * Copyright (C) 2023 Paul Morris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import MuseScore 3.0

MuseScore {
    version:  "4.0"
    description: qsTr("This plugin colors note heads using a two-color 6-6 pitch pattern, where the notes that are the same color belong to the same whole tone scale. (So C, D, E are one color and F, G, A, B are another color.) Colors are applied to the whole score or to the current selection (if there is one). Running the plugin again removes the colors.")
    // MuseScore 3.2.3 chokes on the title, categoryCode, and thumbnail:
    // "Cannot assign to non-existent property"
    // title: "Color Notes: 6-6 Pitch Pattern"
    // categoryCode: "color-notes"
    // thumbnailName: "color_notes_six_six.png"
    menuPath: "Plugins.Notes.Color Notes 6-6 Pitch Pattern"

    id: colorNotesSixSixPitchPattern

    Component.onCompleted: {
        if (mscoreMajorVersion >= 4) {
            colorNotesSixSixPitchPattern.title = "Color Notes 6-6 Pitch Pattern";
            colorNotesSixSixPitchPattern.categoryCode = "color-notes";
            // colorNotesSixSixPitchPattern.thumbnailName = "";
        }
    }

    // "#rrggbb" with rr, gg, and bb being the hex values for red, green,
    // and blue, respectively
    property string cdeColor : "#5e50a1" // purple
    property string fgabColor : "#009c95" // turquoise
    property string black : "#000000"

    // "#010101", // black
    // "#3A5FCD", // lighter blue ink

    // "#333333", // grey 
    // "#27408B", // blue ink

    // Boomwhacker colors.
    // "#e21c48", // C
    // "#f26622", // C#/Db
    // "#f99d1c", // D
    // "#ffcc33", // D#/Eb
    // "#fff32b", // E
    // "#bcd85f", // F
    // "#62bc47", // F#/Gb
    // "#009c95", // G
    // "#0071bb", // G#/Ab
    // "#5e50a1", // A
    // "#8d5ba6", // A#/Bb
    // "#cf3e96"  // B

    /**
     * Color all the note heads (elements with pitch) in the current selection
     * or, if nothing is selected, in the entire score. If most of the notes
     * are already colored then remove the colors, restoring them to black.
     */
    function colorNotes() {
        var fullScore = !curScore.selection.elements.length;
        if (fullScore) {
            cmd("select-all");
        }
        curScore.startCmd();

        var elements = curScore.selection.elements;

        // Determine whether to apply colors or remove them.
        var noteColorCounter = 0;
        
        for (var i in elements) {
            if (elements[i].pitch) {
                // The element is a note.
                // Use `==` because `===` does not work here.
                noteColorCounter += (elements[i].color == black ? -1 : 1);

                if (noteColorCounter > 30 || noteColorCounter < -30) {
                    break;
                }
            }
        }

        var remove = noteColorCounter > 0;

        // Apply or remove colors.
        if (remove) {
            for (var i in elements) {
                if (elements[i].pitch) {
                    elements[i].color = black;
                }
            }
        } else {
            for (var i in elements) {
                if (elements[i].pitch) {
                    elements[i].color = (elements[i].pitch % 2) === 0
                        ? cdeColor
                        : fgabColor
                }
            }
        }

        curScore.endCmd();
        if (fullScore) {
            cmd("escape");
        }
    }

    onRun: {
        console.log("Begin: Color Notes 6-6 Pitch Pattern");

        colorNotes();

        if (typeof quit === "undefined") {
            // MuseScore 3
            Qt.quit();
        } else {
            // MuseScore 4
            quit();
        }
    }
}
