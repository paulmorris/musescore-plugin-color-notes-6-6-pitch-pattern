# MuseScore Plugin: Color Notes 6-6 Pitch Pattern

This [MuseScore](https://musescore.org) plugin colors note heads using a
two-color 6-6 pitch pattern, where the notes that are the same color belong to
the same whole tone scale.
(So C, D, E are one color and F, G, A, B are another color.)

Colors are applied to the whole score or to the current selection (if there
is one).
You can remove the colors by running the plugin again.

## Compatibility

Compatible with MuseScore 3 and MuseScore 4.

## Installation and Use

Download this source code repository and copy or move the
`color-notes-6-6-pitch-pattern` directory into MuseScore's `Plugins` directory.
Then when you start MuseScore this plugin should appear as one of the plugins
that you can enable.
Enable this plugin and you should be able to run it from the "Plugins" menu.

## Why a 6-6 pitch pattern?

Having notes that are colored in a 6-6 pitch pattern is especially helpful
for musicians playing an
[isomorphic instrument](https://musicnotation.org/wiki/instruments/isomorphic-instruments/)
or a
[6-6 Colored Traditional Keyboard](https://musicnotation.org/wiki/instruments/6-6-colored-traditional-7-5-keyboard/).
But it also provides benefits to anyone reading music, no matter what
instrument they play.

### Intervals

Interval relationships are more explicit, making it easier to identify
intervals quickly and fully, and that makes it easier to read music by interval
patterns.

For example, when there are no accidental signs, you can immediately see a
difference between major and minor seconds.
The notes in a major second will have the same color while the ones in a
minor second will have a different color.
This kind of visual pattern is present in all the other intervals as well:
thirds, fourths, fifths, sixths, sevenths, octaves, etc.
This makes it easy to see and learn to recognize the inteval patterns commonly
found in music (e.g. scales and chords).

### Key Signatures and Accidenal Signs

If a note is sharp or flat because of the key signature or an accidental sign,
it will have a different color than it does when it is not sharp or flat.
This provides a direct visual reminder to play it as a sharp or flat,
helping to prevent mistakes due to forgetting the key signature or an
accidental sign.

### Where can I learn more?

[Minimal 6-6 Notation System](https://musicnotation.org/wiki/notation-systems/minimal-6-6-notation-system-by-paul-morris/)
goes into more depth on this particular approach of adding a visible 6-6
pattern to traditional music notation.

[The Music Notation Project](https://musicnotation.org/) documents a number
of alternative notation systems that feature a 6-6 pattern in various ways.
In particular see
[6-6 and 7-5 Pitch Patterns](https://musicnotation.org/tutorials/6-6-and-7-5-pitch-patterns/),
[Reading and Playing Music by Intervals](https://musicnotation.org/tutorials/reading-playing-music-intervals/),
and
[Intervals in 6-6 Music Notation Systems](http://musicnotation.org/tutorials/intervals-in-6-6-music-notation-systems/).
One alternative notation system that features a 6-6 pitch pattern is
[Clairnote Music Notation](https://clairnote.org/).
